/*
 * TG4VecGeomNavigator.h
 *
 *  Created on: Mar 1, 2018
 *      Author: swenzel
 */

#ifndef G4VECGEOM_INCLUDE_TG4VECGEOMNAVIGATOR_H_
#define G4VECGEOM_INCLUDE_TG4VECGEOMNAVIGATOR_H_

#include "G4Navigator.hh"
#include <VecGeom/navigation/NavigationState.h>
#include <VecGeom/base/Vector3D.h>
#include "G4ThreeVector.hh"
#include <VecGeom/navigation/GlobalLocator.h>

class TG4VecGeomDetectorConstruction;
struct FastG4VecGeomLookup;

/// \brief GEANT4 navigator using directly a VecGeom geometry.
///
/// All navigation methods required by G4 tracking are implemented by
/// this class by invoking the corresponding functionality of VecGeom
///
/// \author S. Wenzel; CERN

class TG4VecGeomNavigator : public G4Navigator {

protected:
  const FastG4VecGeomLookup *fG4VGLookup; ///< G4VecGeom detector construction
                                          ///< which is the structure keeping geometry lookups etc.

  bool fWouldEnter         = false;                           ///< Next step is entering daughter
  bool fWouldExit          = false;                           ///< Next step is exiting current volume
  G4ThreeVector fNextPoint = G4ThreeVector(-1E8, -1E8, -1E8); ///< Crossing point with next boundary
  // G4ThreeVector       fSafetyOrig;      ///< Last computed safety origin
  // G4double            fLastSafety;      ///< Last computed safety
  int fNzeroSteps                      = 0; ///< Number of zero steps in ComputeStep
  vecgeom::NavigationState *fCurState  = nullptr;
  vecgeom::NavigationState *fTestState = nullptr;

  vecgeom::VPlacedVolume const *fHitCandidate = nullptr;
  bool fForceReInit                           = false;
  bool fOnBoundary                            = false; // if we are currently on boundary

private:
  // G4VPhysicalVolume *SynchronizeHistory();
  // TGeoNode          *SynchronizeGeoManager();

  G4VPhysicalVolume *UpdateInternalHistory();
  void UpdateVecGeomState();
  void PrintState(); // print info about navigatio state (exiting, ...)

  const vecgeom::Vector3D<double> GetLocalVGPoint(G4ThreeVector const &) const;

  // a common relocate method ( to calculate propagated states after the boundary )
  void Relocate(vecgeom::Vector3D<double> const &pointafterboundary, vecgeom::NavigationState &state,
                vecgeom::VPlacedVolume const *entercandidate = nullptr)
  {
    // this means that we are leaving the mother
    // alternatively we could use nextvolumeindex like before
    if (!entercandidate) {
      vecgeom::GlobalLocator::RelocatePointFromPathForceDifferent(pointafterboundary, state);
#ifdef CHECK_RELOCATION_ERRORS
      assert(in_state.Distance(out_state) != 0 && " error relocating when leaving ");
#endif
    } else {
      // continue directly further down ( next volume should have been stored
      // in out_state already )
      vecgeom::GlobalLocator::LocateGlobalPoint(
          entercandidate, entercandidate->GetTransformation()->Transform(pointafterboundary), state, false);
#ifdef CHECK_RELOCATION_ERRORS
      assert(in_state.Distance(out_state) != 0 && " error relocating when entering ");
#endif
      return;
    }
  }

public:
  // TG4VecGeomNavigator() = default;
  TG4VecGeomNavigator(FastG4VecGeomLookup const &dc); // : fDetConstruction(&dc) {}
  ~TG4VecGeomNavigator() override;

  /// Return the navigation history

  // G4NavigationHistory *GetHistory() {return &fHistory;}

  // Virtual methods for navigation
  G4double ComputeStep(const G4ThreeVector &pGlobalPoint, const G4ThreeVector &pDirection,
                       const G4double pCurrentProposedStepLength, G4double &pNewSafety) override;

  G4VPhysicalVolume *ResetHierarchyAndLocate(const G4ThreeVector &point, const G4ThreeVector &direction,
                                             const G4TouchableHistory &h) override;

  G4VPhysicalVolume *LocateGlobalPointAndSetup(const G4ThreeVector &point, const G4ThreeVector *direction = 0,
                                               const G4bool pRelativeSearch = true,
                                               const G4bool ignoreDirection = true) override;

  void LocateGlobalPointWithinVolume(const G4ThreeVector &position) override;

  G4double ComputeSafety(const G4ThreeVector &globalpoint, const G4double pProposedMaxLength,
                         const G4bool keepState) override;

  G4TouchableHistoryHandle CreateTouchableHistoryHandle() const override;

  G4ThreeVector GetLocalExitNormal(G4bool *valid) override;

  G4ThreeVector GetGlobalExitNormal(const G4ThreeVector &point, G4bool *valid) override;

  // Return Exit Surface Normal and validity too.
  // Can only be called if the Navigator's last Step has crossed a
  // volume geometrical boundary.
  // It returns the Normal to the surface pointing out of the volume that
  // was left behind and/or into the volume that was entered.
  // Convention:
  //   The *local* normal is in the coordinate system of the *final* volume.
  // Restriction:
  //   Normals are not available for replica volumes (returns valid= false)
  // These methods takes full care about how to calculate this normal,
  // but if the surfaces are not convex it will return valid=false.

  // helper function to check navigation histories
  void CheckStates() const;
};

inline const vecgeom::Vector3D<double> TG4VecGeomNavigator::GetLocalVGPoint(G4ThreeVector const &p) const
{
  const auto localpoint = fHistory.GetTopTransform().TransformPoint(p);
  return vecgeom::Vector3D<double>(localpoint[0], localpoint[1], localpoint[2]);
}

#endif /* G4VECGEOM_INCLUDE_TG4VECGEOMNAVIGATOR_H_ */
