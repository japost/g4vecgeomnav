/*
 * TG4VecGeomNavigator.h
 *
 *  Created on: Mar 1, 2018
 *      Author: swenzel
 */

#include "TG4VecGeomNavigator.h"

#include <VecGeom/management/GeoManager.h>
#include <VecGeom/navigation/NewSimpleNavigator.h>
#include <VecGeom/navigation/VSafetyEstimator.h>
#include <VecGeom/navigation/GlobalLocator.h>
#include "FastG4VecGeomLookup.h"
#include "TG4VecGeomDetectorConstruction.h"
#include <VecGeom/base/Vector3D.h>
#include <cmath>

/// Return the navigation history

// G4NavigationHistory *GetHistory() {return &fHistory;}

using V3 = vecgeom::Vector3D<double>;

TG4VecGeomNavigator::TG4VecGeomNavigator(FastG4VecGeomLookup const &dc) : G4Navigator(), fG4VGLookup(&dc)
{
  std::cout << "INIT TG4VecGeomNavigator sizeof(G4Navigator) " << sizeof(G4Navigator) << " sizeof(VecGeomNavi) "
            << sizeof(TG4VecGeomNavigator) << "\n";
}

G4VPhysicalVolume *TG4VecGeomNavigator::UpdateInternalHistory()
{
  // updates fHistory to be consistent with G4 logical geometry
  // very crude thing ... needs to be optimize
  fHistory.Clear();
  assert(fHistory.GetDepth() == 0);
  assert(fHistory.GetTopVolume() == nullptr);
  for (int l = 0; l < fCurState->GetCurrentLevel(); ++l) {
    int replica    = 0;
    bool isreplica = false;
    auto g4pv      = fG4VGLookup->VGToG4(fCurState->At(l)->id(), replica, isreplica);
    // std::cerr << "GOT REPLICA " << replica << "\n";

    // auto g4pv = fG4VGLookup->fPV_VGtoG4_Vector[fCurState->At(l)->id()];
    if (l == 0) {
      assert(isreplica == false);
      fHistory.SetFirstEntry(g4pv);
    } else {
      if (isreplica) {
        // unfortunately, we need to set the transform of the replica here
        // inspired from G4ReplicaNavigation::ComputeTransformation()
        const auto &tr = fCurState->At(l)->GetTransformation();
        g4pv->SetTranslation(G4ThreeVector(tr->Translation(0), tr->Translation(1), tr->Translation(2)));
        fHistory.NewLevel(g4pv, kReplica, replica);
      } else {
        fHistory.NewLevel(g4pv);
      }
    }
  }
  //  TG4VecGeomDetectorConstruction::CheckVolumeConsistency(fCurState->Top(),
  //  fHistory.GetTopVolume());
  //  if ( fCurState->Top() != nullptr ){
  //    assert(fG4VGLookup->fPV_VGtoG4_Vector[fCurState->Top()->id()] ==
  //    fHistory.GetTopVolume());
  //  }
  //  else {
  //	assert(nullptr == fHistory.GetTopVolume());
  //  }
  // assert(fHistory.GetDepth() == fCurState->GetCurrentLevel() - 1);
  return fHistory.GetTopVolume();
}

void TG4VecGeomNavigator::UpdateVecGeomState()
{
  fCurState->Clear();
  for (int l = 0; l <= fHistory.GetDepth(); ++l) {
    // TODO: generalize for case of lookup
    // TODO: how to get the copy number?
    // const auto pv = fG4VGLookup->fPV_G4toVG_Vector[((MYG4VP*)fHistory.GetVolume(l))->getID()];
    // const int replica = 0;
    const int replica  = fHistory.GetReplicaNo(l);
    const int replica2 = (replica == -1) ? 0 : replica;
    const auto pv      = fG4VGLookup->G4ToVG(((MYG4VP *)fHistory.GetVolume(l))->getID(), replica2);
    fCurState->Push(pv);
  }
  // TG4VecGeomDetectorConstruction::CheckVolumeConsistency(fCurState->Top(), fHistory.GetTopVolume());
}

void Inspect(G4VPhysicalVolume const *currentNode, G4ThreeVector const &globalpoint, G4ThreeVector const &globaldir,
             G4NavigationHistory const &fHistory, std::ostream &outstream = std::cout)
{
  auto localpoint = fHistory.GetTopTransform().TransformPoint(globalpoint);
  auto localdir   = fHistory.GetTopTransform().TransformAxis(globaldir);

  outstream << "INSPECTNAV --- START ------\n";
  outstream << "Navigating in placed volume : " << currentNode->GetName() << "\n";
  outstream << "localpoint " << localpoint << "\n";
  outstream << "localdir " << localdir << "\n";

  const auto lvol  = currentNode->GetLogicalVolume();
  const auto shape = lvol->GetSolid();
  double rootstep  = shape->DistanceToOut(localpoint, localdir);
  auto di          = shape->DistanceToIn(localpoint, localdir);
  outstream << "DistanceToOutMother G4 : " << rootstep << " ( " << di << " )\n";
  outstream << "Containment in mother " << shape->Inside(localpoint) << "\n";
  outstream << "ITERATING OVER " << lvol->GetNoDaughters() << " DAUGHTER VOLUMES "
            << "\n";
  for (int d = 0; d < lvol->GetNoDaughters(); ++d) {
    const auto daughter = lvol->GetDaughter(d);

    const auto daughtershape = daughter->GetLogicalVolume()->GetSolid();
    G4AffineTransform trans2(daughter->GetRotation(), daughter->GetTranslation());
    G4AffineTransform trans(trans2.Invert()); // WHY??

    auto ddistance   = daughtershape->DistanceToIn(trans.TransformPoint(localpoint), trans.TransformAxis(localdir));
    auto outdistance = daughtershape->DistanceToOut(trans.TransformPoint(localpoint), trans.TransformAxis(localdir));

    outstream << "DistanceToDaughter G$ : " << daughter->GetName()
              << "("
              //              << daughtershape->ClassName() << ")"
              << " " << ddistance << " "
              << " CONTAINED " << daughtershape->Inside(trans.TransformPoint(localpoint)) << " TOOUT " << outdistance
              << "\n";
  }
}

G4double TG4VecGeomNavigator::ComputeStep(const G4ThreeVector &pGlobalPoint, const G4ThreeVector &pDirection,
                                          const G4double pCurrentProposedStepLength, G4double &pNewSafety)
{
  static int iStep = 0;
  iStep++;
  pNewSafety = 0; // make sure this variable is initialized

  // std::cerr << "CS called";
  // get the best navigator for the current volume
  const auto curpv            = fCurState->Top();
  const auto curlevel         = fCurState->GetCurrentLevel();
  const auto bestnav          = curpv->GetLogicalVolume()->GetNavigator();
  const bool indicateEntering = true;

  auto &os = std::cout;
  //  if(iStep < 127500 && iStep >= 127430) {
  //	  vecgeom::SimpleNavigator nav;
  //	  nav.InspectEnvironmentForPointAndDirection(
  //			  V3(pGlobalPoint[0], pGlobalPoint[1], pGlobalPoint[2]),
  //		      V3(pDirection[0], pDirection[1], pDirection[2]), *fCurState);
  //	  Inspect(fHistory.GetTopVolume(), pGlobalPoint, pDirection, fHistory, std::cout);
  //     if(iStep == 127499) exit(1);
  //  }

  auto step   = bestnav->ComputeStepAndSafety(V3(pGlobalPoint[0], pGlobalPoint[1], pGlobalPoint[2]),
                                            V3(pDirection[0], pDirection[1], pDirection[2]), pCurrentProposedStepLength,
                                            *fCurState, !fOnBoundary, pNewSafety, indicateEntering);
  fOnBoundary = false;

  if (pNewSafety < 0) {
    // std::cerr << "NEGATIVE SAFETY\n";
    pNewSafety = 0;
  }

  const auto state_changed = fCurState->GetCurrentLevel() > curlevel;
  // determine entering or leaving situation (or none)
  if (step < pCurrentProposedStepLength) // geometry step
  {
    fWouldExit  = !state_changed;
    fWouldEnter = !fWouldExit;
    fNextPoint  = pGlobalPoint + step * pDirection;
  } else {
    fWouldExit  = false;
    fWouldEnter = false;
    step        = kInfinity;
  }

  if (state_changed) {
    fHitCandidate = fCurState->Top();
    fCurState->Pop();
  } else {
    fHitCandidate = nullptr;
  }

  // assert(step >= 0.);
  if (step < 0) {
    step = 0;
    // fCurState->Pop();
    // UpdateInternalHistory();
    fNzeroSteps = 0;
    // Giving a tiny push
    assert(fCurState->GetCurrentLevel() == curlevel);
    fForceReInit = true;
    return 1E-3;
  } else if (step < 1E-10) {
    // std::cerr << "ZERO step " << fNzeroSteps << "\n";
    fNzeroSteps++;
    if (fNzeroSteps > 4) {
      // we are stuck ... going up in history
      // and giving tiny extra push
      // fCurState->Pop();
      // UpdateInternalHistory();
      // fNzeroSteps = 0;
      // Giving a tiny push and force a reinit of location
      fForceReInit = true;
      assert(fCurState->GetCurrentLevel() == curlevel);
      return 1E-3;
    }
  } else {
    fForceReInit = false;
    fNzeroSteps  = 0;
  }
  // std::cerr << " " << step << " WE " << fWouldEnter << " WEXIT " <<
  // fWouldExit << "\n";
  assert(fCurState->GetCurrentLevel() == curlevel);
  return step;
}

G4VPhysicalVolume *TG4VecGeomNavigator::ResetHierarchyAndLocate(const G4ThreeVector &point,
                                                                const G4ThreeVector &direction,
                                                                const G4TouchableHistory &h)
{
  // this function is supposedly called to set the navigator
  // back to a backed-up state
  // This might occur when the engine was interrupted to follow
  // a secondary before following the mother to the end
  // std::cerr << "RESET called\n";
  fEnteredDaughter = false;
  fExitedMother    = false;
  fWouldEnter      = false;
  fWouldExit       = false;
  fHistory         = *h.GetHistory();
  UpdateVecGeomState();
  return fHistory.GetTopVolume();
}

void TG4VecGeomNavigator::PrintState()
{
  // std::cout << "ExM" << fExitedMother << " EnD " << fEnteredDaughter << " GEOM " << fWasLimitedByGeometry << "\n";
}

G4VPhysicalVolume *TG4VecGeomNavigator::LocateGlobalPointAndSetup(const G4ThreeVector &point,
                                                                  const G4ThreeVector *direction,
                                                                  const G4bool pRelativeSearch,
                                                                  const G4bool ignoreDirection)
{
  // TODO: treat relative search
  if (!fCurState) {
    fCurState  = vecgeom::NavigationState::MakeInstance(vecgeom::GeoManager::Instance().getMaxDepth() + 1);
    fTestState = vecgeom::NavigationState::MakeInstance(vecgeom::GeoManager::Instance().getMaxDepth() + 1);
  }

  // THIS SHOULD ALSO BE GIVEN BY fLastGeomStepWasGeometry
  bool onBoundary = fWasLimitedByGeometry;
  if (fWouldEnter || fWouldExit) {
    const auto dsqr = point.diff2(fNextPoint);
    if (dsqr < 1.e-16) {
      //         fNextPoint = globalPoint;
      onBoundary = true;
    }
#ifdef G4ROOT_DEBUG
    if (onBoundary)
      G4cout << "   ON BOUNDARY "
             << "entering/exiting = " << fStepEntering << "/" << fStepExiting << G4endl;
    else
      G4cout << "   IN VOLUME   "
             << "entering/exiting = " << fStepEntering << "/" << fStepExiting << G4endl;
#endif
  }
  // std::cerr << " GEOMFLAG " << fWasLimitedByGeometry << " BOUND " << onBoundary << " DIR " << !ignoreDirection <<
  // "\n";

  double extra    = onBoundary ? 1e-6 : 0.;
  const auto &mgr = vecgeom::GeoManager::Instance();
  V3 p(point[0], point[1], point[2]);
  if (direction) {
    p += V3(extra * direction->x(), extra * direction->y(), extra * direction->z());
  }
  if (fForceReInit || !pRelativeSearch) {
    fCurState->Clear();
    vecgeom::GlobalLocator::LocateGlobalPoint(mgr.GetWorld(), p, *fCurState, true);
    if (fForceReInit) {
      // fCurState->Pop();
      fForceReInit = false;
    }
  } else {
    if (onBoundary) {
      // Relocate!
      G4ThreeVector sp(point.x() + extra * direction->x(), point.y() + extra * direction->y(),
                       point.z() + extra * direction->z());
      auto localpoint = fHistory.GetTopTransform().TransformPoint(sp);

      const auto checksumbefore = fCurState->getCheckSum();
      Relocate(vecgeom::Vector3D<double>(localpoint[0], localpoint[1], localpoint[2]), *fCurState, fHitCandidate);
      const auto checksumafter = fCurState->getCheckSum();
      if (direction && (checksumbefore == checksumafter)) {
        std::cerr << "FAILED TO RELOCATE\n";
      }
      // compare this with global locate
      //    auto checkState = vecgeom::NavigationState::MakeInstance(
      //        vecgeom::GeoManager::Instance().getMaxDepth()+1);
      //    vecgeom::GlobalLocator::LocateGlobalPoint(mgr.GetWorld(), p,
      //    *checkState,
      //                                              true);
      //    if(checkState->Top() != fCurState->Top()) {
      //       std::cerr << "inconsistency detected\n";
      //    }
      //
      //    vecgeom::NavigationState::ReleaseInstance(checkState);
    }
  }
  auto r = UpdateInternalHistory();
  if (onBoundary) {
    fExitedMother    = fWouldExit;
    fEnteredDaughter = fWouldEnter;
    fOnBoundary      = true;
  }
  // CheckStates();
  return r;
}

void TG4VecGeomNavigator::LocateGlobalPointWithinVolume(const G4ThreeVector &position)
{
  // std::cerr << "LPWV called\n";
  fWouldEnter      = false;
  fWouldExit       = false;
  fExitedMother    = false;
  fEnteredDaughter = false;
  fOnBoundary      = false;
}

G4double TG4VecGeomNavigator::ComputeSafety(const G4ThreeVector &globalpoint, const G4double pProposedMaxLength,
                                            const G4bool keepState)
{
  if (fOnBoundary) {
    //  std::cerr << "exiting due to boundary check 1\n";
    return 0.;
  }
  if (fEnteredDaughter || fExitedMother) {
    //   std::cerr << "exiting due to boundary check 2\n";
    return 0.;
  }
  // using the VecGeomNav local states
  if (fWouldEnter || fWouldExit) {
    // std::cerr << "exiting due to boundary check 3\n";
    return 0.;
  }
  const auto bestsafetyestimator = fCurState->Top()->GetLogicalVolume()->GetSafetyEstimator();
  const auto safety =
      bestsafetyestimator->ComputeSafety(V3(globalpoint[0], globalpoint[1], globalpoint[2]), *fCurState);

  // NOTE: The currentstate may not be appropriate for this globalpoint
  // This is why ROOT is actually resetting the hierarchy here
  // Check what G4 is doing as well!!
  if (safety < 0.) {
    std::cerr << "Negative safety for " << globalpoint[0] << " " << globalpoint[1] << " " << globalpoint[2] << " "
              << bestsafetyestimator->GetName() << "\n";
    fCurState->Print();
    fTestState->Clear();
    const auto &mgr = vecgeom::GeoManager::Instance();
    vecgeom::GlobalLocator::LocateGlobalPoint(mgr.GetWorld(), V3(globalpoint[0], globalpoint[1], globalpoint[2]),
                                              *fTestState, true);
    fTestState->Print();
  }
  // std::cerr << "returning " << safety << "\n";
  return std::max(0., safety);
}

G4TouchableHistoryHandle TG4VecGeomNavigator::CreateTouchableHistoryHandle() const
{
  std::cerr << "Create touchable handle called (NOT IMPLEMENTED)\n";
  return G4TouchableHistoryHandle();
}

G4ThreeVector TG4VecGeomNavigator::GetLocalExitNormal(G4bool *valid)
{
  std::cerr << "LOCAL EXIT NORMAL CALLED (NOT IMPLEMENTED)\n";
  return G4ThreeVector();
}

G4ThreeVector TG4VecGeomNavigator::GetGlobalExitNormal(const G4ThreeVector &point, G4bool *valid)
{
  // std::cerr << "GLOBAL EXIT NORMAL CALLED\n";
  // transform point to local coordinate
  auto localpoint = fHistory.GetTopTransform().TransformPoint(point);

  // then call Normal on local point and last volume
  auto pvol = fCurState->Top();
  vecgeom::Vector3D<double> p(localpoint[0], localpoint[1], localpoint[2]);
  vecgeom::Vector3D<double> n;
  auto v = pvol->Normal(p, n);
  *valid = true;

  return G4ThreeVector(n[0], n[1], n[2]);
}

TG4VecGeomNavigator::~TG4VecGeomNavigator()
{
  std::cout << "Made " << fNzeroSteps << "0 steps\n";
}

void TG4VecGeomNavigator::CheckStates() const
{
  G4ThreeVector g4point(1, 1, 0.5);
  auto p1 = GetLocalVGPoint(g4point);

  vecgeom::Vector3D<double> vgpoint(1, 1, 0.5);
  vecgeom::Transformation3D m;
  fCurState->TopMatrix(m);
  auto p2 = m.Transform(vgpoint);

  if (std::abs(p1[0] - p2[0]) > 1E-6) {
    std::cerr << "ERROR\n";
  }
  if (std::abs(p1[1] - p2[1]) > 1E-6) {
    std::cerr << "ERROR\n";
  }
  if (std::abs(p1[2] - p2[2]) > 1E-6) {
    std::cerr << "ERROR\n";
  }

  // access the G4AffineTransformations and calculate difference
  std::vector<G4AffineTransform> transforms;
  G4NavigationHistory hist(fHistory);
  while (hist.GetDepth() > 0) {
    transforms.emplace_back(hist.GetTopTransform());
    hist.BackLevel();
  }

  if (transforms.size() >= 2) {
    auto diff = transforms[0] * transforms[1].Inverse();
  }
}
