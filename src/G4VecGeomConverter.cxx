/*
 * G4VecGeomConverter.cxx
 *
 *  Created on: Mar 16, 2018
 *      Author: swenzel
 */

#include "G4VecGeomConverter.h"
// -- fill lookups in detector construction
#include "FastG4VecGeomLookup.h"

#include <VecGeom/base/Transformation3D.h>
#include <VecGeom/base/Stopwatch.h>
#include <VecGeom/management/GeoManager.h>
#include <VecGeom/navigation/NavigationState.h>
#include <VecGeom/navigation/NewSimpleNavigator.h>
#include <VecGeom/navigation/SimpleABBoxNavigator.h>
#include <VecGeom/navigation/SimpleABBoxLevelLocator.h>
#include <VecGeom/navigation/VoxelLevelLocator.h>
#include <VecGeom/navigation/HybridLevelLocator.h>
#include <VecGeom/navigation/HybridNavigator2.h>
#include <VecGeom/management/HybridManager2.h>
#include <VecGeom/management/FlatVoxelManager.h>
#include <VecGeom/navigation/SimpleABBoxSafetyEstimator.h>
#include <VecGeom/navigation/HybridSafetyEstimator.h>
#include <VecGeom/navigation/VoxelSafetyEstimator.h>

#include <VecGeom/volumes/LogicalVolume.h>
#include <VecGeom/volumes/PlacedRootVolume.h>
#include <VecGeom/volumes/PlacedVolume.h>
#include <VecGeom/volumes/UnplacedBox.h>
#include <VecGeom/volumes/UnplacedTube.h>
#include <VecGeom/volumes/UnplacedCone.h>
#include <VecGeom/volumes/UnplacedRootVolume.h>
#include <VecGeom/volumes/UnplacedParaboloid.h>
#include <VecGeom/volumes/UnplacedParallelepiped.h>
#include <VecGeom/volumes/UnplacedPolyhedron.h>
#include <VecGeom/volumes/UnplacedTrd.h>
#include <VecGeom/volumes/UnplacedOrb.h>
#include <VecGeom/volumes/UnplacedSphere.h>
#include <VecGeom/volumes/UnplacedBooleanVolume.h>
#include <VecGeom/volumes/UnplacedTorus2.h>
#include <VecGeom/volumes/UnplacedTrapezoid.h>
#include <VecGeom/volumes/UnplacedPolycone.h>
#include <VecGeom/volumes/UnplacedScaledShape.h>
#include <VecGeom/volumes/UnplacedGenTrap.h>
#include <VecGeom/volumes/UnplacedSExtruVolume.h>
#include <VecGeom/volumes/UnplacedExtruded.h>
#include <VecGeom/volumes/PlanarPolygon.h>
#include <VecGeom/volumes/UnplacedAssembly.h>
#include <VecGeom/volumes/UnplacedCutTube.h>
#include <VecGeom/volumes/UnplacedTet.h>
#include <VecGeom/volumes/UnplacedGenericPolycone.h>

//#include "TGeoManager.h"
#include "G4VPhysicalVolume.hh"
#include "G4PVParameterised.hh"
#include <G4PVDivision.hh>
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4Sphere.hh"
#include "G4Tubs.hh"
#include "G4CutTubs.hh"
#include "G4Cons.hh"
#include "G4Para.hh"
#include "G4Trd.hh"
#include "G4Tet.hh"
#include "G4Trap.hh"
#include "G4Orb.hh"
#include "G4Torus.hh"
#include "G4BooleanSolid.hh"
#include "G4IntersectionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4UnionSolid.hh"
#include "G4Polycone.hh"
#include "G4Polyhedra.hh"
#include "G4DisplacedSolid.hh"
#include "G4Transform3D.hh"
#include "G4AffineTransform.hh"
#include "G4ExtrudedSolid.hh"
#include "G4ReflectedSolid.hh"
#include "G4GenericPolycone.hh"
#include "G4VisExtent.hh"
// more stuff might be needed

#include "G4RunManager.hh"
#include "G4TransportationManager.hh"
#include "G4PropagatorInField.hh"
#include "TG4VecGeomNavigator.h"
#include "TG4VecGeomIncNavigator.h"
#include "TG4VecGeomVoxelNavigation.h"
#include "G4Navigator.hh"

#include <iostream>
#include <list>
#include <cassert>
#include "TString.h"
#include "TError.h"

using namespace vecgeom;

// we implement a missing method of G4Navigator
void G4Navigator::SetVoxelNavigation(G4VoxelNavigation *nav)
{
  fpvoxelNav = nav;
}

double TrapParametersGetZ(G4Trap const &t)
{
  const double *start = reinterpret_cast<const double *>(&t);

  // 10 derives from sizeof(G4VSolid) + ... + offset
  auto r = start[10];
  assert(r == t.GetZHalfLength());
  return r;
}
void TrapParametersGetOriginalThetaAndPhi(G4Trap const &t, double &theta, double &phi)
{
  const double *start = reinterpret_cast<const double *>(&t);
  auto x              = start[10]; // tan(theta)*cos(phi)
  auto y              = start[11]; // tan(theta)*sin(phi)

  if (x == 0. && y == 0.) {
    theta = 0.;
    phi   = 0.;
    return;
  }
  // try to catch more corner cases + requirement that theta > 0??

  // tan(t) = x / cos(phi) --> y = x / cos(phi) * sin(phi) = x * tan(phi)
  phi   = atan(y / x);
  theta = atan(x / cos(phi));
}

void InitVecGeomNavigators()
{
  for (auto &lvol : vecgeom::GeoManager::Instance().GetLogicalVolumesMap()) {
    if (lvol.second->GetDaughtersp()->size() < 4) {
      lvol.second->SetNavigator(NewSimpleNavigator<>::Instance());
    }
    if (lvol.second->GetDaughtersp()->size() >= 5) {
      lvol.second->SetNavigator(SimpleABBoxNavigator<>::Instance());
      lvol.second->SetSafetyEstimator(SimpleABBoxSafetyEstimator::Instance());
    }
    if (lvol.second->GetDaughtersp()->size() >= 10) {
      lvol.second->SetNavigator(HybridNavigator<>::Instance());
      lvol.second->SetSafetyEstimator(VoxelSafetyEstimator::Instance());
      lvol.second->SetLevelLocator(TVoxelLevelLocator<false>::GetInstance());
      HybridManager2::Instance().InitStructure((lvol.second));
      FlatVoxelManager::Instance().InitStructure((lvol.second));
    }

    if (lvol.second->ContainsAssembly()) {
      lvol.second->SetLevelLocator(SimpleAssemblyAwareABBoxLevelLocator::GetInstance());
    } else {
      if (lvol.second->GetLevelLocator() == nullptr) {
        lvol.second->SetLevelLocator(SimpleABBoxLevelLocator::GetInstance());
      }
    }
  }
}

#define VECGEOM_VECTORAPI

class GenericPlacedVolume : public vecgeom::VPlacedVolume {
public:
  using Base = vecgeom::VPlacedVolume;
  // using Base::Base;
  GenericPlacedVolume(char const *const label, LogicalVolume const *const logicalVolume,
                      Transformation3D const *const transformation)
      : Base(label, logicalVolume, transformation)
  {
  }

  GenericPlacedVolume(LogicalVolume const *const logicalVolume, Transformation3D const *const transformation)
      : GenericPlacedVolume("", logicalVolume, transformation)
  {
  }

  virtual int MemorySize() const override { return sizeof(*this); }
  virtual void PrintType() const override { PrintType(std::cout); }
  virtual void PrintType(std::ostream &os) const override {}

  virtual void PrintImplementationType(std::ostream &os) const override {}
  virtual void PrintUnplacedType(std::ostream &os) const override {}
  virtual bool Contains(Vector3D<Precision> const &point) const override
  {
    return GetUnplacedVolume()->Contains(GetTransformation()->Transform(point));
  }
  virtual void Contains(SOA3D<Precision> const &point, bool *const output) const override {}
  virtual bool Contains(Vector3D<Precision> const &point, Vector3D<Precision> &localPoint) const override
  {
    assert(false);
    return false;
  }

  virtual bool UnplacedContains(Vector3D<Precision> const &localPoint) const override
  {
    return GetUnplacedVolume()->Contains(localPoint);
  }

  virtual EnumInside Inside(Vector3D<Precision> const &point) const override
  {
    return GetUnplacedVolume()->Inside(GetTransformation()->Transform(point));
  }

  virtual void Inside(SOA3D<Precision> const &point, Inside_t *const output) const override {}

  virtual Precision SafetyToIn(Vector3D<Precision> const &position) const override
  {
    return GetUnplacedVolume()->SafetyToIn(GetTransformation()->Transform(position));
  }

  virtual Precision DistanceToIn(Vector3D<Precision> const &position, Vector3D<Precision> const &direction,
                                 const Precision step_max = kInfLength) const override
  {
    return GetUnplacedVolume()->DistanceToIn(GetTransformation()->Transform(position),
                                             GetTransformation()->TransformDirection(position), step_max);
  }

#ifdef VECGEOM_VECTORAPI
  // if we have any SIMD backend, we offer a SIMD interface
  virtual Real_v DistanceToInVec(Vector3D<Real_v> const &position, Vector3D<Real_v> const &direction,
                                 const Real_v step_max = kInfLength) const override
  {
    return Real_v{0.};
  }
#endif

  virtual void DistanceToIn(SOA3D<Precision> const &position, SOA3D<Precision> const &direction,
                            Precision const *const step_max, Precision *const output) const override
  {
  }

  // to be deprecated
  virtual void DistanceToInMinimize(SOA3D<Precision> const &position, SOA3D<Precision> const &direction,
                                    int daughterindex, Precision *const output, int *const nextnodeids) const
  {
  }

  VECCORE_ATT_HOST_DEVICE
  virtual Precision DistanceToOut(Vector3D<Precision> const &position, Vector3D<Precision> const &direction,
                                  Precision const step_max = kInfLength) const override
  {
    return GetUnplacedVolume()->DistanceToOut(position, direction, step_max);
  }

#ifdef VECGEOM_VECTORAPI
  // define this interface in case we don't have the Scalar interface

  virtual Real_v DistanceToOutVec(Vector3D<Real_v> const &position, Vector3D<Real_v> const &direction,
                                  Real_v const step_max = kInfLength) const override
  {
    return Real_v{0.};
  }
#endif

  VECCORE_ATT_HOST_DEVICE
  virtual Precision PlacedDistanceToOut(Vector3D<Precision> const &position, Vector3D<Precision> const &direction,
                                        Precision const step_max = kInfLength) const override
  {
    assert(false);
    return 0;
  }

  virtual void DistanceToOut(SOA3D<Precision> const &position, SOA3D<Precision> const &direction,
                             Precision const *const step_max, Precision *const output) const override
  {
  }

  virtual void DistanceToOut(SOA3D<Precision> const &position, SOA3D<Precision> const &direction,
                             Precision const *const step_max, Precision *const output,
                             int *const nextnodeindex) const override
  {
  }

#ifdef VECGEOM_VECTORAPI
  virtual Real_v SafetyToInVec(Vector3D<Real_v> const &position) const override { return Real_v{0.}; }
#endif
  virtual void SafetyToIn(SOA3D<Precision> const &position, Precision *const safeties) const override {}
  virtual void SafetyToInMinimize(SOA3D<Precision> const &points, Precision *const safeties) const override {}
  virtual Precision SafetyToOut(Vector3D<Precision> const &position) const override
  {
    return GetUnplacedVolume()->SafetyToOut(position);
  }
#ifdef VECGEOM_VECTORAPI
  virtual Real_v SafetyToOutVec(Vector3D<Real_v> const &position) const override { return Real_v{0.}; }
#endif
  virtual void SafetyToOut(SOA3D<Precision> const &position, Precision *const safeties) const override{};
  virtual void SafetyToOutMinimize(SOA3D<Precision> const &points, Precision *const safeties) const {};
  virtual Precision SurfaceArea() const override { return GetUnplacedVolume()->SurfaceArea(); }
  virtual VPlacedVolume const *ConvertToUnspecialized() const override { return nullptr; }

  /*virtual*/ TGeoShape const *ConvertToRoot() const /*override*/ { return nullptr; }

  VECCORE_ATT_HOST_DEVICE
  virtual void Extent(Vector3D<Precision> &min, Vector3D<Precision> &max) const override
  {
    return GetUnplacedVolume()->Extent(min, max);
  }

  VECCORE_ATT_HOST_DEVICE
  virtual bool Normal(Vector3D<Precision> const &point, Vector3D<Precision> &normal) const override
  {
    return GetUnplacedVolume()->Normal(GetTransformation()->Transform(point), normal);
  }

  // VECCORE_ATT_HOST_DEVICE
  virtual Precision Capacity() override { return GetUnplacedVolume()->Capacity(); }
};

template <typename S>
class VecGeomG4Solid : public vecgeom::VUnplacedVolume {
public:
  VecGeomG4Solid(S const *g4solid) : fG4Solid(g4solid) {}

  static G4ThreeVector ToG4V(Vector3D<double> const &p) { return G4ThreeVector(p[0], p[1], p[2]); }

  static EnumInside ConvertEnum(::EInside p)
  {
    if (p == ::EInside::kInside) return EnumInside::kInside;
    if (p == ::EInside::kSurface) return EnumInside::kSurface;
    return EnumInside::kOutside;
  }

  // ---------------- Contains --------------------------------------------------------------------
  VECCORE_ATT_HOST_DEVICE
  bool Contains(Vector3D<Precision> const &p) const override
  {
    auto a = ConvertEnum(fG4Solid->Inside(ToG4V(p)));
    if (a == EnumInside::kOutside) return false;
    return true;
  }

  VECCORE_ATT_HOST_DEVICE
  EnumInside Inside(Vector3D<Precision> const &p) const override { return ConvertEnum(fG4Solid->Inside(ToG4V(p))); }

  // ---------------- DistanceToOut functions -----------------------------------------------------
  VECCORE_ATT_HOST_DEVICE
  Precision DistanceToOut(Vector3D<Precision> const &p, Vector3D<Precision> const &d,
                          Precision step_max = kInfLength) const override
  {
    return fG4Solid->DistanceToOut(ToG4V(p), ToG4V(d), step_max);
  }

  // the USolid/GEANT4-like interface for DistanceToOut (returning also exiting normal)
  VECCORE_ATT_HOST_DEVICE
  Precision DistanceToOut(Vector3D<Precision> const &p, Vector3D<Precision> const &d, Vector3D<Precision> &normal,
                          bool &convex, Precision step_max = kInfLength) const override
  {
    assert(false);
    return 0.;
  }

  // ---------------- SafetyToOut functions -----------------------------------------------------
  VECCORE_ATT_HOST_DEVICE
  Precision SafetyToOut(Vector3D<Precision> const &p) const override { return fG4Solid->DistanceToOut(ToG4V(p)); }

  //#ifdef VECGEOM_VECTORAPI
  // an explicit SIMD interface
  // VECCORE_ATT_HOST_DEVICE
  // Real_v SafetyToOutVec(Vector3D<Real_v> const &p) const override {
  //   return Real_v(0);
  // }
  // #endif

  // ---------------- DistanceToIn functions -----------------------------------------------------
  VECCORE_ATT_HOST_DEVICE
  Precision DistanceToIn(Vector3D<Precision> const &p, Vector3D<Precision> const &d,
                         const Precision step_max = kInfLength) const override
  {
    return fG4Solid->DistanceToIn(ToG4V(p), ToG4V(d));
  }

  // ---------------- SafetyToIn functions -------------------------------------------------------
  VECCORE_ATT_HOST_DEVICE
  Precision SafetyToIn(Vector3D<Precision> const &p) const override { return fG4Solid->DistanceToIn(ToG4V(p)); }

  // ---------------- Normal ---------------------------------------------------------------------

  VECCORE_ATT_HOST_DEVICE
  bool Normal(Vector3D<Precision> const &p, Vector3D<Precision> &normal) const override
  {
    auto n = fG4Solid->SurfaceNormal(ToG4V(p));
    normal = Vector3D<double>(n[0], n[1], n[2]);
    return true;
  }

  // ----------------- Extent --------------------------------------------------------------------
  VECCORE_ATT_HOST_DEVICE
  void Extent(Vector3D<Precision> &aMin, Vector3D<Precision> &aMax) const override
  {
    auto ext = fG4Solid->GetExtent();
    aMin.Set(ext.GetXmin(), ext.GetYmin(), ext.GetZmin());
    aMax.Set(ext.GetXmax(), ext.GetYmax(), ext.GetZmax());
  }

  double Capacity() const override { return const_cast<S *>(fG4Solid)->S::GetCubicVolume(); }

  double SurfaceArea() const override { return const_cast<S *>(fG4Solid)->S::GetSurfaceArea(); }

  int MemorySize() const override { return sizeof(this); }
  void Print(std::ostream &os) const override
  {
    if (fG4Solid) {
      os << *fG4Solid;
    }
  }
  void Print() const override { Print(std::cout); }
  G4GeometryType GetEntityType() const { return fG4Solid->GetEntityType(); }

  vecgeom::VPlacedVolume *SpecializedVolume(LogicalVolume const *const volume,
                                            Transformation3D const *const transformation,
                                            const TranslationCode trans_code, const RotationCode rot_code,
                                            VPlacedVolume *const placement = nullptr) const override
  {

    return new GenericPlacedVolume(volume, transformation);
  }

private:
  const S *fG4Solid;
};

#if 0
void GenericPlacedVolume::PrintType(std::ostream &os) const {
  if( auto g4solidUV=  dynamic_cast<VecGeomG4Solid<G4VSolid>>(GetLogicalVolume()->GetUnplacedVolume() )
  { os << g4solid.GetEntityType(); }
}
#endif

void G4VecGeomConverter::ConvertG4Geometry(G4VPhysicalVolume const *worldg4)
{
  Clear();
  GeoManager::Instance().Clear();
  Stopwatch timer;
  timer.Start();
  auto volumes = Convert(worldg4);
  assert(volumes->size() == 1);
  fWorld = (*volumes)[0];
  timer.Stop();
  if (fVerbose) {
    std::cout << "*** Conversion of G4 -> VecGeom finished (" << timer.Elapsed() << " s) ***\n";
  }
  GeoManager::Instance().SetWorld(fWorld);
  timer.Start();
  GeoManager::Instance().CloseGeometry();
  timer.Stop();
  if (fVerbose) {
    std::cout << "*** Closing VecGeom geometry finished (" << timer.Elapsed() << " s) ***\n";
  }
  fWorld = GeoManager::Instance().GetWorld();

  //
  // setup navigator; by
  //

  timer.Start();
  fFastG4VGLookup.InitSize(VPlacedVolume::GetIdCount());
  auto iter = fPlacedVolumeMap.begin();
  for (; iter != fPlacedVolumeMap.end(); ++iter) {
    auto placedvols = iter->first;
    auto g4pl       = iter->second;
    fFastG4VGLookup.Insert(placedvols, g4pl);
  }
  timer.Stop();
  std::cout << "*** Setup of syncing lookup structure finished (" << timer.Elapsed() << " s) ***\n";

  // making the navigator
  auto nav =
#ifdef REPLACE_FULL_NAVIGATOR
      new TG4VecGeomIncNavigator(fFastG4VGLookup); // new G4Navigator();
#else
      new G4Navigator();
  nav->SetVoxelNavigation(new TG4VecGeomVoxelNavigation(fFastG4VGLookup));
#endif

  // hooking the navigator
  G4TransportationManager *trMgr = G4TransportationManager::GetTransportationManager();
  assert(trMgr);
  trMgr->SetNavigatorForTracking(nav);
  G4FieldManager *fieldMgr = trMgr->GetPropagatorInField()->GetCurrentFieldManager();
  delete trMgr->GetPropagatorInField();
  trMgr->SetPropagatorInField(new G4PropagatorInField(nav, fieldMgr));
  trMgr->ActivateNavigator(nav);
  G4EventManager *evtMgr = G4EventManager::GetEventManager();
  if (evtMgr) {
    evtMgr->GetTrackingManager()->GetSteppingManager()->SetNavigator(nav);
  }
  Info("SetNavigator", "TG4VecGeomNavigator created and registered to G4TransportationManager");
  // attaching special VecGeom navigators
  timer.Start();
  InitVecGeomNavigators();
  timer.Stop();
  std::cout << "*** Setup of VecGeom navigators finished (" << timer.Elapsed() << " s) ***\n";
}

void G4VecGeomConverter::ExtractReplicatedTransformations(G4PVReplica const &replica,
                                                          std::vector<vecgeom::Transformation3D const *> &transf) const
{
  // read out parameters
  EAxis axis;
  int nReplicas;
  double width;
  double offset;
  bool consuming;
  replica.GetReplicationData(axis, nReplicas, width, offset, consuming);
  std::cout << axis << " " << nReplicas << " " << width << " " << offset << " " << consuming << "\n";
  assert(offset == 0.);
  // for the moment only replication along x,y,z get translation
  Vector3D<double> direction;
  switch (axis) {
  case kXAxis: {
    direction.Set(1, 0, 0);
    break;
  }
  case kYAxis: {
    direction.Set(0, 1, 0);
    break;
  }
  case kZAxis: {
    direction.Set(0, 0, 1);
    break;
  }
  default: {
    std::cerr << "UNSUPPORTED REPLICATION\n";
  }
  }
  for (int r = 0; r < nReplicas; ++r) {
    const auto translation = (-width * (nReplicas - 1) * 0.5 + r * width) * direction;
    auto tr                = new vecgeom::Transformation3D(translation[0], translation[1], translation[2]);
    transf.push_back(tr);
  }
}

std::vector<VPlacedVolume const *> const *G4VecGeomConverter::Convert(G4VPhysicalVolume const *node)
{
  // WARN POTENTIALLY UNSUPPORTED CASE
  if (dynamic_cast<const G4PVParameterised *>(node)) {
    std::cout << "WARNING: PARAMETRIZED VOLUME FOUND " << node->GetName() << "\n";
  }
  fReplicaTransformations.clear();
  if (auto replica = dynamic_cast<const G4PVReplica *>(node)) {
    std::cout << "INFO: REPLICA VOLUME FOUND " << node->GetName() << "\n";
#ifdef ACTIVATEREPLICATION
    ExtractReplicatedTransformations(*replica, fReplicaTransformations);
#endif
  }
  if (dynamic_cast<const G4PVDivision *>(node)) {
    std::cout << "WARNING: DIVISION VOLUME FOUND " << node->GetName() << "\n";
  }

  if (fPlacedVolumeMap.Contains(node)) {
    assert(false); // for the moment unsupported
    return GetPlacedVolume(node);
  }

  // convert node transformation
  const auto transformation = Convert(node->GetTranslation(), node->GetRotation());
  if (fReplicaTransformations.size() == 0) {
    fReplicaTransformations.emplace_back(transformation);
  }

  auto vgvector = new std::vector<VPlacedVolume const *>;

  const auto g4logical          = node->GetLogicalVolume();
  LogicalVolume *logical_volume = Convert(g4logical);

  // place (all replicas here) ... if normal we will only have one transformation
  for (auto &transf : fReplicaTransformations) {
    const VPlacedVolume *placed_volume = logical_volume->Place(node->GetName(), transf);
    vgvector->emplace_back(placed_volume);
  }

  int remaining_daughters = 0;
  {
    // All or no daughters should have been placed already
    remaining_daughters = g4logical->GetNoDaughters() - logical_volume->GetDaughters().size();
    assert(remaining_daughters <= 0 || remaining_daughters == (int)g4logical->GetNoDaughters());
  }

  for (int i = 0; i < remaining_daughters; ++i) {
    const auto node         = g4logical->GetDaughter(i);
    const auto placedvector = Convert(node);
    for (auto placed : *placedvector) {
      logical_volume->PlaceDaughter(const_cast<VPlacedVolume *const>(placed));
    }
  }

  fPlacedVolumeMap.Set(node, vgvector);
  return vgvector;
}

Transformation3D *G4VecGeomConverter::Convert(G4ThreeVector const &t, G4RotationMatrix const *rot)
{
  // if (fTransformationMap.Contains(geomatrix)) return const_cast<Transformation3D *>(fTransformationMap[geomatrix]);
  Transformation3D *transformation;
  if (!rot) {
    transformation = new Transformation3D(t[0], t[1], t[2]);
  } else {
    // transformation = new Transformation3D(
    //    t[0], t[1], t[2], rot->xx(), rot->xy(), rot->xz(), rot->yx(), rot->yy(),
    //    rot->yz(), rot->zx(), rot->zy(), rot->zz());
    transformation = new Transformation3D(t[0], t[1], t[2], rot->xx(), rot->yx(), rot->zx(), rot->xy(), rot->yy(),
                                          rot->zy(), rot->xz(), rot->yz(), rot->zz());
  }
  // transformation->FixZeroes();
  // transformation->SetProperties();
  // fTransformationMap.Set(geomatrix, transformation);
  return transformation;
}

LogicalVolume *G4VecGeomConverter::Convert(G4LogicalVolume const *volume)
{
  if (fLogicalVolumeMap.Contains(volume)) return const_cast<LogicalVolume *>(fLogicalVolumeMap[volume]);

  VUnplacedVolume const *unplaced;
  unplaced                            = Convert(volume->GetSolid());
  LogicalVolume *const logical_volume = new LogicalVolume(volume->GetName().c_str(), unplaced);
  fLogicalVolumeMap.Set(volume, logical_volume);

  // can be used to make a cross check for dimensions and other properties
  // make a cross check using cubic volume property
  //  if (!dynamic_cast<UnplacedScaledShape const *>(
  //          logical_volume->GetUnplacedVolume()) &&
  //      !dynamic_cast<G4BooleanSolid const *>(
  //          volume->GetSolid())) {
  //    const auto v1 = logical_volume->GetUnplacedVolume()->Capacity();
  //    const auto v2 = volume->GetSolid()->GetCubicVolume();
  //    std::cerr << "v1 " << v1 << " " << v2 << "\n";
  //
  //    assert(v1 > 0.);
  //    assert(std::abs(v1 - v2) / v1 < 0.05);
  //  }
  return logical_volume;
}

// the inverse: here we need both the placed volume and logical volume as input
// they should match
// TGeoVolume *G4VecGeomConverter::Convert(VPlacedVolume const *const placed_volume, LogicalVolume const *const
// logical_volume)
//{
//  assert(placed_volume->GetLogicalVolume() == logical_volume);
//
//  if (fLogicalVolumeMap.Contains(logical_volume)) return const_cast<TGeoVolume *>(fLogicalVolumeMap[logical_volume]);
//
//  const TGeoShape *root_shape = placed_volume->ConvertToRoot();
//  // Some shapes do not exist in ROOT: we need to protect for that
//  if (!root_shape) return nullptr;
//  TGeoVolume *geovolume = new TGeoVolume(logical_volume->GetLabel().c_str(), /* the name */
//                                         root_shape, 0                       /* NO MATERIAL FOR THE MOMENT */
//                                         );
//
//  fLogicalVolumeMap.Set(geovolume, logical_volume);
//  return geovolume;
//}

VUnplacedVolume *G4VecGeomConverter::Convert(G4VSolid const *shape)
{
  if (fUnplacedVolumeMap.Contains(shape)) return const_cast<VUnplacedVolume *>(fUnplacedVolumeMap[shape]);
  VUnplacedVolume *unplaced_volume = nullptr;

  //
  // THE BOX
  if (auto box = dynamic_cast<G4Box const *>(shape)) {
    unplaced_volume =
        GeoManager::MakeInstance<UnplacedBox>(box->GetXHalfLength(), box->GetYHalfLength(), box->GetZHalfLength());
  }

  // THE POLYCONE
  if (auto p = dynamic_cast<G4Polycone const *>(shape)) {
    auto params = p->GetOriginalParameters();
    // fix dimensions - (requires making a copy of some arrays)
    const int NZs = params->Num_z_planes;
    double zs[NZs];
    double rmins[NZs];
    double rmaxs[NZs];
    for (int i = 0; i < NZs; ++i) {
      zs[i]    = params->Z_values[i];
      rmins[i] = params->Rmin[i];
      rmaxs[i] = params->Rmax[i];
    }
    unplaced_volume =
        GeoManager::MakeInstance<UnplacedPolycone>(params->Start_angle, params->Opening_angle, NZs, zs, rmins, rmaxs);
  }

  // Polyhedra
  if (auto pgon = dynamic_cast<G4Polyhedra const *>(shape)) {
    auto params = pgon->GetOriginalParameters();
    // G4 has a different radius conventions (than TGeo, gdml, VecGeom)!
    const double convertRad = std::cos(0.5 * params->Opening_angle / params->numSide);

    // fix dimensions - (requires making a copy of some arrays)
    const int NZs = params->Num_z_planes;
    double zs[NZs];
    double rmins[NZs];
    double rmaxs[NZs];
    for (int i = 0; i < NZs; ++i) {
      zs[i]    = params->Z_values[i];
      rmins[i] = params->Rmin[i] * convertRad;
      rmaxs[i] = params->Rmax[i] * convertRad;
    }

    auto phistart = params->Start_angle;
    while (phistart < 0.) {
      phistart += M_PI * 2.;
    }

    unplaced_volume = GeoManager::MakeInstance<UnplacedPolyhedron>(phistart, params->Opening_angle, params->numSide,
                                                                   NZs, zs, rmins, rmaxs);
  }

  // THE TUBESEG
  if (auto tube = dynamic_cast<G4Tubs const *>(shape)) {
    unplaced_volume =
        GeoManager::MakeInstance<UnplacedTube>(tube->GetInnerRadius(), tube->GetOuterRadius(), tube->GetZHalfLength(),
                                               tube->GetStartPhiAngle(), tube->GetDeltaPhiAngle());
  }

  //
  // THE CONESEG
  if (auto cone = dynamic_cast<G4Cons const *>(shape)) {
    unplaced_volume = GeoManager::MakeInstance<UnplacedCone>(
        cone->GetInnerRadiusMinusZ(), cone->GetOuterRadiusMinusZ(), cone->GetInnerRadiusPlusZ(),
        cone->GetOuterRadiusPlusZ(), cone->GetZHalfLength(), cone->GetStartPhiAngle(), cone->GetDeltaPhiAngle());
  }

  // THE TORUS
  if (auto torus = dynamic_cast<G4Torus const *>(shape)) {
    unplaced_volume = GeoManager::MakeInstance<UnplacedTorus2>(torus->GetRmin(), torus->GetRmax(), torus->GetRtor(),
                                                               torus->GetSPhi(), torus->GetDPhi());
  }

  // TRD
  if (auto trd = dynamic_cast<G4Trd const *>(shape)) {
    unplaced_volume =
        GeoManager::MakeInstance<UnplacedTrd>(trd->GetXHalfLength1(), trd->GetXHalfLength2(), trd->GetYHalfLength1(),
                                              trd->GetYHalfLength2(), trd->GetZHalfLength());
  }

  // TRAPEZOID
  if (auto p = dynamic_cast<G4Trap const *>(shape)) {
    std::cerr << "TRAP " << p->GetName();
    // unplaced_volume = new VecGeomG4Solid<G4Trap>(p);
    double theta;
    double phi;
    TrapParametersGetOriginalThetaAndPhi(*p, theta, phi);
    std::cerr << " THETA " << theta << " PHI " << phi << "\n";
    unplaced_volume = GeoManager::MakeInstance<UnplacedTrapezoid>(
        TrapParametersGetZ(*p), theta, phi, p->GetYHalfLength1(), p->GetXHalfLength1(), p->GetXHalfLength2(),
        p->GetTanAlpha1(), p->GetYHalfLength2(), p->GetXHalfLength3(), p->GetXHalfLength4(), p->GetTanAlpha2());
  }

  if (auto boolean = dynamic_cast<G4BooleanSolid const *>(shape)) {
    // the "right" shape should be a G4DisplacedSolid which holds the matrix
    if (dynamic_cast<G4DisplacedSolid const *>(boolean->GetConstituentSolid(0))) {
      assert(false);
    }
    if (!dynamic_cast<G4DisplacedSolid const *>(boolean->GetConstituentSolid(1))) {
      assert(false);
    }
    G4VSolid const *left  = boolean->GetConstituentSolid(0);
    G4VSolid const *right = boolean->GetConstituentSolid(1);

    G4VSolid *rightraw = nullptr;
    G4AffineTransform g4righttrans;

    if (auto displaced = dynamic_cast<G4DisplacedSolid const *>(right)) {
      rightraw     = displaced->GetConstituentMovedSolid();
      g4righttrans = displaced->GetTransform().Invert();
    }

    // need the matrix;
    Transformation3D const *lefttrans  = &vecgeom::Transformation3D::kIdentity;
    auto rot                           = g4righttrans.NetRotation();
    Transformation3D const *righttrans = Convert(g4righttrans.NetTranslation(), &rot);

    // unplaced shapes
    VUnplacedVolume const *leftunplaced  = Convert(left);
    VUnplacedVolume const *rightunplaced = Convert(rightraw);

    assert(leftunplaced != nullptr);
    assert(rightunplaced != nullptr);

    //    // the problem is that I can only place logical volumes
    VPlacedVolume *const leftplaced  = (new LogicalVolume("inner_virtual", leftunplaced))->Place(lefttrans);
    VPlacedVolume *const rightplaced = (new LogicalVolume("inner_virtual", rightunplaced))->Place(righttrans);

    if (dynamic_cast<G4SubtractionSolid const *>(boolean)) {
      unplaced_volume =
          GeoManager::MakeInstance<UnplacedBooleanVolume<kSubtraction>>(kSubtraction, leftplaced, rightplaced);
    } else if (dynamic_cast<G4IntersectionSolid const *>(boolean)) {
      unplaced_volume =
          GeoManager::MakeInstance<UnplacedBooleanVolume<kIntersection>>(kIntersection, leftplaced, rightplaced);
    } else if (dynamic_cast<G4UnionSolid const *>(boolean)) {
      unplaced_volume = GeoManager::MakeInstance<UnplacedBooleanVolume<kUnion>>(kUnion, leftplaced, rightplaced);
    } else {
      assert(false);
    }
  }

  if (auto p = dynamic_cast<G4ReflectedSolid const *>(shape)) {
    auto t = p->GetDirectTransform3D();
    if (t.getTranslation().mag2() == 0. && (t.xx() == -1. || t.yy() == -1. || t.zz() == -1.)) {
      // std::cerr << "SIMPLE REFLECTION --> CONVERT TO SCALED SHAPE\n";
      VUnplacedVolume *referenced_shape = Convert(p->GetConstituentMovedSolid());

      // implement in terms of scaled shape first of all
      // we could later modify the node directly?
      unplaced_volume = GeoManager::MakeInstance<UnplacedScaledShape>(referenced_shape, t.xx(), t.yy(), t.zz());
    } else {
      std::cerr << "NONSIMPLE REFLECTION\n";
      unplaced_volume = new VecGeomG4Solid<G4ReflectedSolid>(p);
    }
  }

  //  // THE PARABOLOID
  //  if (shape->IsA() == TGeoParaboloid::Class()) {
  //    TGeoParaboloid const *const p = static_cast<TGeoParaboloid const
  //    *>(shape);
  //
  //    unplaced_volume =
  //    GeoManager::MakeInstance<UnplacedParaboloid>(p->GetRlo() * LUnit(),
  //    p->GetRhi() * LUnit(),
  //                                                                   p->GetDz()
  //                                                                   *
  //                                                                   LUnit());
  //  }
  //

#ifdef TRIAL_PARA
  // Doesn't compile with current G4 --> TO BE ACTIVATED LATER ON
  if (auto pp = dynamic_cast<G4Para const *>(shape)) {
    unplaced_volume = GeoManager::MakeInstance<UnplacedParallelepiped>(
        pp->GetXHalfLength(), pp->GetYHalfLength(), pp->GetZHalfLength(),
        std::atan(pp->GetTanAlpha()), // pp->GetOriginalAlpha(),
        pp->GetOriginalTheta(), pp->GetOriginalPhi());
  }
#endif
  if (auto orb = dynamic_cast<G4Orb const *>(shape)) {
    unplaced_volume = GeoManager::MakeInstance<UnplacedOrb>(orb->GetRadius());
  }
  if (auto sphr = dynamic_cast<G4Sphere const *>(shape)) {
    unplaced_volume = GeoManager::MakeInstance<UnplacedSphere>(sphr->GetInnerRadius(), sphr->GetOuterRadius(),
                                                               sphr->GetStartPhiAngle(), sphr->GetDeltaPhiAngle(),
                                                               sphr->GetStartThetaAngle(), sphr->GetDeltaThetaAngle());
  }
  if (auto gp = dynamic_cast<G4GenericPolycone const *>(shape)) {
    // auto params = p->GetOriginalParameters();
    // fix dimensions - (requires making a copy of some arrays)
    const int nRZs = gp->GetNumRZCorner();
    double zs[nRZs];
    double rs[nRZs];
    for (int i = 0; i < nRZs; ++i) {
      G4PolyconeSideRZ rzCorner = gp->GetCorner(i);
      zs[i]                     = rzCorner.z;
      rs[i]                     = rzCorner.r;
    }
    unplaced_volume = GeoManager::MakeInstance<UnplacedGenericPolycone>(
        gp->GetStartPhi(), gp->GetEndPhi() - gp->GetStartPhi(), nRZs, zs, rs);
  }
  if (auto tet = dynamic_cast<G4Tet const *>(shape)) {
    G4ThreeVector anchor, p1, p2, p3;
    tet->GetVertices(anchor, p1, p2, p3);
    // Else use std::vector<G4ThreeVector> vertices = tet->GetVertices();
    const Vector3D<Precision> pt0(anchor.getX(), anchor.getY(), anchor.getZ());
    const Vector3D<Precision> pt1(p1.getX(), p1.getY(), p1.getZ());
    const Vector3D<Precision> pt2(p2.getX(), p2.getY(), p2.getZ());
    const Vector3D<Precision> pt3(p3.getX(), p3.getY(), p3.getZ());
    unplaced_volume = GeoManager::MakeInstance<UnplacedTet>(pt0, pt1, pt2, pt3);
  }
#if 0
  if (auto ph = dynamic_cast<G4Polyhedra const *>(shape)) {
    // auto params = p->GetOriginalParameters();
    // fix dimensions - (requires making a copy of some arrays)
    const int nRZs = ph->GetNumRZCorner();
    double zs[nRZs];
    double rs[nRZs];
    for (int i = 0; i < nRZs; ++i) {
      G4PolyhedraSideRZ rzCorner= ph->GetCorner(i);
      zs[i] = rzCorner.z;
      rs[i] = rzCorner.r;
    }
    unplaced_volume =
        GeoManager::MakeInstance<UnplacedPolyhedron>(ph->GetStartPhi(), ph->GetEndPhi()-ph->GetStartPhi(), nRZs, zs, rs);
  }
#endif
  //  if (shape->IsA() == TGeoCompositeShape::Class()) {
  //    TGeoCompositeShape const *const compshape =
  //    static_cast<TGeoCompositeShape const *>(shape);
  //    TGeoBoolNode const *const boolnode        = compshape->GetBoolNode();
  //
  //    // need the matrix;
  //    Transformation3D const *lefttrans  =
  //    Convert(boolnode->GetLeftMatrix());
  //    Transformation3D const *righttrans =
  //    Convert(boolnode->GetRightMatrix());
  //
  //    auto transformationadjust = [this](Transformation3D *tr, TGeoShape
  //    const
  //    *shape) {
  //      // TODO: combine this with external method
  //      Transformation3D adjustment;
  //      if (shape->IsA() == TGeoBBox::Class()) {
  //        TGeoBBox const *const box = static_cast<TGeoBBox const *>(shape);
  //        auto o                    = box->GetOrigin();
  //        if (o[0] != 0. || o[1] != 0. || o[2] != 0.) {
  //          adjustment = Transformation3D::kIdentity;
  //          adjustment.SetTranslation(o[0] * LUnit(), o[1] * LUnit(), o[2] *
  //          LUnit());
  //          adjustment.SetProperties();
  //          tr->MultiplyFromRight(adjustment);
  //          tr->SetProperties();
  //        }
  //      }
  //    };
  //    // adjust transformation in case of shifted boxes
  //    transformationadjust(const_cast<Transformation3D *>(lefttrans),
  //    boolnode->GetLeftShape());
  //    transformationadjust(const_cast<Transformation3D *>(righttrans),
  //    boolnode->GetRightShape());
  //
  //    // unplaced shapes
  //    VUnplacedVolume const *leftunplaced  =
  //    Convert(boolnode->GetLeftShape());
  //    VUnplacedVolume const *rightunplaced =
  //    Convert(boolnode->GetRightShape());
  //
  //    assert(leftunplaced != nullptr);
  //    assert(rightunplaced != nullptr);
  //
  //    // the problem is that I can only place logical volumes
  //    VPlacedVolume *const leftplaced  = (new LogicalVolume("inner_virtual",
  //    leftunplaced))->Place(lefttrans);
  //    VPlacedVolume *const rightplaced = (new LogicalVolume("inner_virtual",
  //    rightunplaced))->Place(righttrans);
  //
  //    // now it depends on concrete type
  //    if (boolnode->GetBooleanOperator() == TGeoBoolNode::kGeoSubtraction) {
  //      unplaced_volume =
  //          GeoManager::MakeInstance<UnplacedBooleanVolume<kSubtraction>>(kSubtraction,
  //          leftplaced, rightplaced);
  //    } else if (boolnode->GetBooleanOperator() ==
  //    TGeoBoolNode::kGeoIntersection) {
  //      unplaced_volume =
  //          GeoManager::MakeInstance<UnplacedBooleanVolume<kIntersection>>(kIntersection,
  //          leftplaced, rightplaced);
  //    } else if (boolnode->GetBooleanOperator() == TGeoBoolNode::kGeoUnion)
  //    {
  //      unplaced_volume =
  //      GeoManager::MakeInstance<UnplacedBooleanVolume<kUnion>>(kUnion,
  //      leftplaced, rightplaced);
  //    }
  //  }
  //

  //
  //  // THE SCALED SHAPE
  //  if (shape->IsA() == TGeoScaledShape::Class()) {
  //    TGeoScaledShape const *const p = static_cast<TGeoScaledShape const
  //    *>(shape);
  //    // First convert the referenced shape
  //    VUnplacedVolume *referenced_shape = Convert(p->GetShape());
  //    const double *scale_root          = p->GetScale()->GetScale();
  //
  //    unplaced_volume =
  //        GeoManager::MakeInstance<UnplacedScaledShape>(referenced_shape,
  //        scale_root[0], scale_root[1], scale_root[2]);
  //  }
  //
  //  // THE ELLIPTICAL TUBE AS SCALED TUBE
  //--if (auto ct = dynamic_cast<G4EllipticalTubs const *>(shape)) {
  //  if (shape->IsA() == TGeoEltu::Class()) {
  //    TGeoEltu const *const p = static_cast<TGeoEltu const *>(shape);
  //    // Create the corresponding unplaced tube, with:
  //    // rmin=0, rmax=A, dz=dz, which is scaled with (1., A/B, 1.)
  //    UnplacedTube *tubeUnplaced =
  //        GeoManager::MakeInstance<UnplacedTube>(0, p->GetA() * LUnit(),
  //        p->GetDZ() * LUnit(), 0, kTwoPi);
  //
  //    unplaced_volume =
  //    GeoManager::MakeInstance<UnplacedScaledShape>(tubeUnplaced, 1.,
  //    p->GetB() / p->GetA(), 1.);
  //  }
  //
  //  // THE ARB8
  //  if (shape->IsA() == TGeoArb8::Class() || shape->IsA() ==
  //  TGeoGtra::Class()) {
  //    TGeoArb8 *p     = (TGeoArb8 *)(shape);
  //    unplaced_volume = ToUnplacedGenTrap(p);
  //  }
  //
  //  // THE SIMPLE/GENERAL XTRU
  //  if (shape->IsA() == TGeoXtru::Class()) {
  //    TGeoXtru *p = (TGeoXtru *)(shape);
  //    // analyse convertability: the shape must have 2 planes with the same
  //    scaling
  //    // and offsets to make a simple extruded
  //    size_t Nvert = (size_t)p->GetNvert();
  //    size_t Nsect = (size_t)p->GetNz();
  //    if (Nsect == 2 && p->GetXOffset(0) == p->GetXOffset(1) &&
  //    p->GetYOffset(0) == p->GetYOffset(1) &&
  //        p->GetScale(0) == p->GetScale(1)) {
  //      double *x = new double[Nvert];
  //      double *y = new double[Nvert];
  //      for (size_t i = 0; i < Nvert; ++i) {
  //        // Normally offsets should be 0 and scales should be 1, but just
  //        to
  //        be safe
  //        x[i] = LUnit() * (p->GetXOffset(0) + p->GetX(i) * p->GetScale(0));
  //        y[i] = LUnit() * (p->GetYOffset(0) + p->GetY(i) * p->GetScale(0));
  //      }
  //      unplaced_volume =
  //      GeoManager::MakeInstance<UnplacedSExtruVolume>(p->GetNvert(), x, y,
  //      LUnit() * p->GetZ(0),
  //                                                                       LUnit()
  //                                                                       *
  //                                                                       p->GetZ(1));
  //      delete[] x;
  //      delete[] y;
  //    }
  //#ifndef VECGEOM_CUDA_INTERFACE
  //    else {
  //      // Make the general extruded solid.
  //      XtruVertex2 *vertices = new XtruVertex2[Nvert];
  //      XtruSection *sections = new XtruSection[Nsect];
  //      for (size_t i = 0; i < Nvert; ++i) {
  //        vertices[i].x = p->GetX(i);
  //        vertices[i].y = p->GetY(i);
  //      }
  //      for (size_t i = 0; i < Nsect; ++i) {
  //        sections[i].fOrigin.Set(p->GetXOffset(i), p->GetYOffset(i),
  //        p->GetZ(i));
  //        sections[i].fScale = p->GetScale(i);
  //      }
  //      unplaced_volume = GeoManager::MakeInstance<UnplacedExtruded>(Nvert,
  //      vertices, Nsect, sections);
  //      delete[] vertices;
  //      delete[] sections;
  //    }
  //#endif
  //  }

  // THE CUT TUBE
  if (auto ct = dynamic_cast<G4CutTubs const *>(shape)) {
    G4ThreeVector lowNorm = ct->GetLowNorm();
    G4ThreeVector hiNorm  = ct->GetHighNorm();
    unplaced_volume       = GeoManager::MakeInstance<UnplacedCutTube>(
        ct->GetInnerRadius(), ct->GetOuterRadius(), ct->GetZHalfLength(), ct->GetStartPhiAngle(),
        ct->GetDeltaPhiAngle(), Vector3D<Precision>(lowNorm[0], lowNorm[1], lowNorm[2]),
        Vector3D<Precision>(hiNorm[0], hiNorm[1], hiNorm[2]));
    // TODO: consider moving this as a specialization to UnplacedTube
  }

  // New volumes should be implemented here...
  if (!unplaced_volume) {
    if (true) { // fVerbose) {
      printf("Unsupported shape for G4 solid %s, of type %s\n", shape->GetName().c_str(),
             shape->GetEntityType().c_str());
    }
    unplaced_volume = new VecGeomG4Solid<G4VSolid>(shape);
    std::cout << " capacity = " << unplaced_volume->Capacity() << "\n";
  }

  fUnplacedVolumeMap.Set(shape, unplaced_volume);
  return unplaced_volume;
}

void G4VecGeomConverter::PrintNodeTable() const
{
  //  for (auto iter : fPlacedVolumeMap) {
  //    std::cerr << iter.first << " " << iter.second << "\n";
  //    TGeoNode const *n = iter.second;
  //    n->Print();
  //  }
}

void G4VecGeomConverter::Clear()
{
  fPlacedVolumeMap.Clear();
  fUnplacedVolumeMap.Clear();
  fLogicalVolumeMap.Clear();
  if (GeoManager::Instance().GetWorld() == fWorld) {
    GeoManager::Instance().SetWorld(nullptr);
  }
}
