//
// Created by Sandro Wenzel on 25.09.19.
//

#include "TG4VecGeomVoxelNavigation.h"

#include "FastG4VecGeomLookup.h"
#include "TG4VecGeomDetectorConstruction.h"
#include <VecGeom/base/Vector3D.h>
#include <VecGeom/navigation/VNavigator.h>
#include <VecGeom/navigation/VLevelLocator.h>
#include <VecGeom/navigation/HybridSafetyEstimator.h>
#undef NDEBUG
#include <cassert>

// For comparisons of answers
#include "G4VPhysicalVolume.hh"
#include "G4VoxelSafety.hh"
#include "G4NormalNavigation.hh"

// To undertake extra comparisons in this case
#include <VecGeom/navigation/NewSimpleNavigator.h>

TG4VecGeomVoxelNavigation::TG4VecGeomVoxelNavigation(FastG4VecGeomLookup const &dc) : fG4VGLookup(&dc) {}

/*
G4double
G4VoxelNavigation::ComputeStep( const G4ThreeVector& localPoint,
                                const G4ThreeVector& localDirection,
                                const G4double currentProposedStepLength,
                                      G4double& newSafety,
                                      G4NavigationHistory& history,
                                      G4bool& validExitNormal,
                                      G4ThreeVector& exitNormal,
                                      G4bool& exiting,
                                      G4bool& entering,
                                      G4VPhysicalVolume *(*pBlockedPhysical),
                                      G4int& blockedReplicaNo )
{
  G4VPhysicalVolume *motherPhysical, *samplePhysical, *blockedExitedVol=0;
  G4LogicalVolume *motherLogical;
  G4VSolid *motherSolid;
  G4ThreeVector sampleDirection;
  G4double ourStep=currentProposedStepLength, ourSafety;
  G4double motherSafety, motherStep=DBL_MAX;
  G4int localNoDaughters, sampleNo;

  G4bool initialNode, noStep;
  G4SmartVoxelNode *curVoxelNode;
  G4int curNoVolumes, contentNo;
  G4double voxelSafety;

  motherPhysical = history.GetTopVolume();
  motherLogical = motherPhysical->GetLogicalVolume();
  motherSolid = motherLogical->GetSolid();

  //
  // Compute mother safety
  //

  motherSafety = motherSolid->DistanceToOut(localPoint);
  ourSafety = motherSafety;                 // Working isotropic safety

  //
  // Compute daughter safeties & intersections
  //

  // Exiting normal optimisation
  //
  if ( exiting && validExitNormal )
  {
    if ( localDirection.dot(exitNormal)>=kMinExitingNormalCosine )
    {
      // Block exited daughter volume
      //
      blockedExitedVol = *pBlockedPhysical;
      ourSafety = 0;
    }
  }
  exiting = false;
  entering = false;

  // For extra checking,  get the distance to Mother early !!
  G4bool   motherValidExitNormal= false;
  G4ThreeVector motherExitNormal(0.0, 0.0, 0.0);

  localNoDaughters = motherLogical->GetNoDaughters();

  fBList.Enlarge(localNoDaughters);
  fBList.Reset();

  initialNode = true;
  noStep = true;

  while (noStep)
  {
    curVoxelNode = fVoxelNode;
    curNoVolumes = curVoxelNode->GetNoContained();
    for (contentNo=curNoVolumes-1; contentNo>=0; contentNo--)
    {
      sampleNo = curVoxelNode->GetVolume(contentNo);
      if ( !fBList.IsBlocked(sampleNo) )
      {
        fBList.BlockVolume(sampleNo);
        samplePhysical = motherLogical->GetDaughter(sampleNo);
        if ( samplePhysical!=blockedExitedVol )
        {
          G4AffineTransform sampleTf(samplePhysical->GetRotation(),
                                     samplePhysical->GetTranslation());
          sampleTf.Invert();
          const G4ThreeVector samplePoint =
                     sampleTf.TransformPoint(localPoint);
          const G4VSolid *sampleSolid     =
                     samplePhysical->GetLogicalVolume()->GetSolid();
          const G4double sampleSafety     =
                     sampleSolid->DistanceToIn(samplePoint);

          if ( sampleSafety<ourSafety )
          {
            ourSafety = sampleSafety;
          }
          if ( sampleSafety<=ourStep )
          {
            sampleDirection = sampleTf.TransformAxis(localDirection);
            G4double sampleStep =
                     sampleSolid->DistanceToIn(samplePoint, sampleDirection);

            if ( sampleStep<=ourStep )
            {
              ourStep = sampleStep;
              entering = true;
              exiting = false;
              *pBlockedPhysical = samplePhysical;
              blockedReplicaNo = -1;
            }
          }
      }
    }
    if (initialNode)
    {
      initialNode = false;
      voxelSafety = ComputeVoxelSafety(localPoint);
      if ( voxelSafety<ourSafety )
      {
        ourSafety = voxelSafety;
      }
      if ( currentProposedStepLength<ourSafety )
      {
        // Guaranteed physics limited
        //
        noStep = false;
        entering = false;
        exiting = false;
        *pBlockedPhysical = 0;
        ourStep = kInfinity;
      }
      else
      {
        //
        // Compute mother intersection if required
        //
        if ( motherSafety<=ourStep )
        {
          if( !fCheck )
          {
            motherStep = motherSolid->DistanceToOut(localPoint, localDirection,
                              true, &motherValidExitNormal, &motherExitNormal);
          }
          // Not correct - unless mother limits step (see below)
          // validExitNormal= motherValidExitNormal;
          // exitNormal= motherExitNormal;
          if( (motherStep >= kInfinity) || (motherStep < 0.0) )
          {
            motherStep = 0.0;
            ourStep = 0.0;
            exiting = true;
            entering = false;

            // validExitNormal= motherValidExitNormal;
            // exitNormal= motherExitNormal;
            // Useful only if the point is very close to surface
            // => but it would need to be rotated to grand-mother ref frame !
            validExitNormal= false;

            *pBlockedPhysical= 0; // or motherPhysical ?
            blockedReplicaNo= 0;  // or motherReplicaNumber ?

            newSafety= 0.0;
            return ourStep;
          }

          if ( motherStep<=ourStep )
          {
            ourStep = motherStep;
            exiting = true;
            entering = false;

            // Exit normal: Natural location to set these;confirmed short step
            //
            validExitNormal= motherValidExitNormal;
            exitNormal= motherExitNormal;

            if ( validExitNormal )
            {
              const G4RotationMatrix *rot = motherPhysical->GetRotation();
              if (rot)
              {
                exitNormal *= rot->inverse();
              }
            }
          }
          else
          {
            validExitNormal = false;
          }
        }
      }
      newSafety = ourSafety;
    }
    if (noStep)
    {
      noStep = LocateNextVoxel(localPoint, localDirection, ourStep);
    }
  }  // end -while (noStep)- loop

  return ourStep;
}
*/

void printState(const char *prefix, G4double safety, G4NavigationHistory &history, G4bool validExitNormal,
                G4ThreeVector const &exitNormal, G4bool exiting, G4bool entering,
                G4VPhysicalVolume *(*pBlockedPhysical), G4int blockedReplicaNo)
{
  std::cout << prefix << " Saf=" << safety << " VEXN:" << validExitNormal << " EN=" << exitNormal
            << " Exit:" << (exiting ? "Y" : "N") << " Enter:" << (entering ? "Y" : "N") << " BLOCKED " << std::setw(15)
            << *pBlockedPhysical << " REPLICA" << blockedReplicaNo << "\n";
  if (*pBlockedPhysical != nullptr) {
    std::cout << "BLOCKED IS " << *pBlockedPhysical << " name: " << (*pBlockedPhysical)->GetName() << "\n";
  }
}

#define G4COMPARE 1

double TG4VecGeomVoxelNavigation::ComputeStep(const G4ThreeVector &localPoint, const G4ThreeVector &localDirection,
                                              const G4double currentProposedStepLength, G4double &newSafety,
                                              G4NavigationHistory &history, G4bool &validExitNormal,
                                              G4ThreeVector &exitNormal, G4bool &exiting, G4bool &entering,
                                              G4VPhysicalVolume *(*pBlockedPhysical), G4int &blockedReplicaNo)
{
  static long counter = 0;
  counter++;

  // return this->G4VoxelNavigation::ComputeStep(localPoint, localDirection, currentProposedStepLength, newSafety,
  // history, validExitNormal, exitNormal, exiting, entering, pBlockedPhysical, blockedReplicaNo);
#if defined(G4COMPARE) && defined(VERBOSE_STEP)
  std::cout << "IN pos:" << localPoint << " dir: " << localDirection << " ";
  printState("IN ", newSafety, history, validExitNormal, exitNormal, exiting, entering, pBlockedPhysical,
             blockedReplicaNo);
#endif
#ifdef G4COMPARE
  G4VPhysicalVolume *blockedPhysicalIn = *pBlockedPhysical;
  int blockedReplicaIn                 = blockedReplicaNo;
#endif
  const int replica  = 0; // fHistory.GetReplicaNo(l);
  const auto g4pvvol = history.GetTopVolume();
  assert(g4pvvol->IsReplicated() == false); // for the moment no replication

  // fetch the VecGeom version of this and dispatch to local VecGeom navigator
  const auto pv                              = fG4VGLookup->G4ToVG(g4pvvol->GetInstanceID(), replica);
  const auto blockedID                       = (*pBlockedPhysical) ? ((MYG4VP *)(*pBlockedPhysical))->getID() : -1;
  const auto currentblocked                  = (*pBlockedPhysical) ? fG4VGLookup->G4ToVG(blockedID, replica) : nullptr;
  const auto lvol                            = pv->GetLogicalVolume();
  const auto bestnav                         = lvol->GetNavigator();
  const bool indicateEntering                = true;
  vecgeom::VPlacedVolume const *hitcandidate = nullptr;

  using V3 = vecgeom::Vector3D<double>;
  const V3 lp(localPoint[0], localPoint[1], localPoint[2]);
  const V3 ld(localDirection[0], localDirection[1], localDirection[2]);
  // vecgeom internal safety
  double safety = 0.;
  if (!(entering || exiting)) {
    // fBestSafety = false;
    auto safetyEst = lvol->GetSafetyEstimator();
    if (safetyEst != nullptr) // && safetyEst->enabled() )
      safety = safetyEst->ComputeSafetyForLocalPoint(lp, pv);
    else {
      // this->G4VoxelNavigation::LocateGlobalPointWithinVolume();
      G4LogicalVolume *g4logical = g4pvvol->GetLogicalVolume();
      // this->G4VoxelNavigation::VoxelLocate( g4logical->GetVoxelHeader(), localPoint );
      safety = this->G4VoxelNavigation::ComputeSafety(localPoint, history, currentProposedStepLength);
    }
    if (safety < vecgeom::kHalfTolerance) {
      safety = 0.0;
    }
  }
  double currentbeststep =
      9e99; // this value indicates that safety was taken and no real distance calculation performed
  double motherstep = 0.;
  if (safety < currentProposedStepLength) {
    motherstep = pv->DistanceToOut(lp, ld);
    if (motherstep < 0) {
      auto inm        = pv->Inside(lp);
      std::string ins = "-Outside-";
      if (inm == vecgeom::cxx::kInside)
        ins = "-Inside-";
      else if (inm == vecgeom::cxx::kSurface) {
        ins = "-Surface-";
      }
      std::cerr << "DistanceToOut negative " << motherstep << "  Reset to Zero.  Inside = " << ins
                << " mother pv= " << pv->GetName() << "\n";
      motherstep = 0.0;
    }
    currentbeststep = std::min(currentProposedStepLength, motherstep);
    bestnav->CheckDaughterIntersections(lvol, lp, ld, currentblocked, currentbeststep, hitcandidate);

    if (currentbeststep < 0.0) {
      std::cout << "CheckDaughterIntersections returned negative distance " << currentbeststep
                << " with candidate vol = " << hitcandidate << " for which inside= " << hitcandidate->Inside(lp)
                << " and distToIn= " << hitcandidate->DistanceToIn(lp, ld) << "\n";
    }
    currentbeststep = std::max(0.0, currentbeststep);
  }

  G4VPhysicalVolume *vgblocked = nullptr;
  bool vgexiting               = false;
  bool vgentering              = false;
  bool vgvalidNormal           = false;
  V3 vgExitNormal(0, 0, 0);
  // G4V

  // determine outstate
  if (currentbeststep < currentProposedStepLength) {
    if (hitcandidate) {
      vgentering = true;
      // block the entering volume
      int replica;
      bool isreplica;
      vgblocked = fG4VGLookup->VGToG4(hitcandidate->id(), replica, isreplica);
      assert(!isreplica);
    } else {
      vgexiting = true;
    }
    // fetch normal if existing (note that funiely we calculate the exit normal from mother even when entering)
    if (vgexiting) {
      // propagated point
      const auto hitpoint = lp + motherstep * ld; //  Was currentbeststep * ld;
      vgvalidNormal       = lvol->GetUnplacedVolume()->Normal(hitpoint, vgExitNormal);
    }
    if (vgentering) {
      // propagated point
      const auto hitpoint = lp + currentbeststep * ld; // Was motherstep * ld;
      // LogicalVolume  hitlog= hitcandidate->GetUnplacedVolume();
      vgvalidNormal = hitcandidate->Normal(hitpoint, vgExitNormal);
      // TODO: Check whether the coordinate system is correct in this call.   It seems not because VPlacedVolume
      //         does not seem to have a method (needed to transform point & forward to unplaced) for this.
    }
  }

#ifdef G4COMPARE
  // we are calling the original method for comparison
  G4LogicalVolume *g4Logical       = g4pvvol->GetLogicalVolume();
  G4SmartVoxelHeader *pVoxelHeader = g4Logical->GetVoxelHeader();
  this->G4VoxelNavigation::VoxelLocate(pVoxelHeader, localPoint);

  auto step = this->G4VoxelNavigation::ComputeStep(localPoint, localDirection, currentProposedStepLength, newSafety,
                                                   history, validExitNormal, exitNormal, exiting, entering,
                                                   pBlockedPhysical, blockedReplicaNo);

  // Compute 'best' safety -- poll all daughters (for comparison)
  G4NormalNavigation normalNav;
  double maxSafety = normalNav.ComputeSafety(localPoint, history);

  double vgstep = (currentbeststep == kInfinity) ? currentProposedStepLength : currentbeststep;
  double g4step = (step == kInfinity) ? currentProposedStepLength : step;
  if (fabs(vgstep - g4step) > 0.0001 * g4step) {

    double diffStep = vgstep - g4step;
    std::cout << "*** DIFFerence seen in call # " << counter << " : ";
    std::cout << "proposed step " << currentProposedStepLength << " VG lstep " << currentbeststep << " VG safety "
              << safety << " || G4 lstep " << step << " G4 safety " << newSafety << " "
              << "// diff-step=" << vgstep - g4step << " diff-safety = " << safety - newSafety;
    std::cout << " Max-safety = " << maxSafety << " max-G4= " << maxSafety - newSafety
              << " max-VG= " << maxSafety - safety;
    std::cout << "\n";
    std::cout << " IN   blocked physVol= " << blockedPhysicalIn << " rep/copy # " << blockedReplicaIn << "\n";
    printState("OUT/g4 ", newSafety, history, validExitNormal, exitNormal, exiting, entering, pBlockedPhysical,
               blockedReplicaNo);
    printState("OUTcmp ", safety, history, vgvalidNormal,
               G4ThreeVector(vgExitNormal[0], vgExitNormal[1], vgExitNormal[2]), vgexiting, vgentering, &vgblocked,
               blockedReplicaNo);
    if (*pBlockedPhysical != vgblocked) {
      if (*pBlockedPhysical)
        std::cout << " G4 candidate: " << (*pBlockedPhysical)->GetName() << " at location "
                  << localPoint + step * localDirection << "\n";
      if (vgblocked)
        std::cout << " VG candidate: " << vgblocked->GetName() << " at location "
                  << localPoint + currentbeststep * localDirection << "\n";
      std::cout << " Current G4-logical volume = " << g4Logical->GetName() << "\n";
    }
  }
  currentbeststep = step; // Let's use the G4 step during testing ...
#else
  // we are not comparing ... so our results count and we have to transfer them
  newSafety         = safety;
  validExitNormal   = vgvalidNormal;
  exitNormal        = G4ThreeVector(vgExitNormal[0], vgExitNormal[1], vgExitNormal[2]);
  exiting           = vgexiting;
  entering          = vgentering;
  *pBlockedPhysical = vgblocked;
  blockedReplicatNo = vgblocked->GetCopyNo(); // good for placements only -- not replicas etc
#endif
  // need to fix the output flags as given by the arguments passed by reference
  // if (currentbeststep < 0) {
  //  std::cerr << " negative step returned " << currentbeststep << " at call " << counter << "\n";
  //}
  return std::max(0., currentbeststep);
}

void printStateForLocate(const char *prefix, bool retEnter, G4NavigationHistory &history,
                         G4ThreeVector const &localPoint)
{
  auto topvol = history.GetTopVolume();
  auto topsol = topvol->GetLogicalVolume()->GetSolid();
  auto inside = topsol->Inside(localPoint);
  std::cout << prefix << " RETURN " << retEnter << (retEnter ? " into Daughter " : " remain/Mother ");
  std::cout << " history.Top() " << topvol << " name: " << std::setw(15) << topvol->GetName() << " copy# "
            << topvol->GetCopyNo();
  if (retEnter) {
    std::cout << " LOCALVECTOR " << localPoint << " ";
    if (inside == kInside)
      std::cout << " -Inside- ";
    else
      std::cout << ((inside == kOutside) ? "-Outside-" : "-Surface-");
  }
  std::cout << "\n";
}

#ifdef G4COMPARE
#define G4LCOMPARE
#endif

bool TG4VecGeomVoxelNavigation::LevelLocate(G4NavigationHistory &history, const G4VPhysicalVolume *blockedVol,
                                            const G4int blockedNum, const G4ThreeVector &globalPoint,
                                            const G4ThreeVector *globalDirection, const G4bool pLocatedOnEdge,
                                            G4ThreeVector &localPoint)
{
#ifdef G4LCOMPARE
  static unsigned long lcounter = 0;
  lcounter++;

  G4ThreeVector localPointIn(localPoint);

  // compare with G4 native call
  G4NavigationHistory historycopy(history);
  G4ThreeVector localPointCopy(localPoint);
  bool returnvalue = this->G4VoxelNavigation::LevelLocate(historycopy, blockedVol, blockedNum, globalPoint,
                                                          globalDirection, pLocatedOnEdge, localPointCopy);
  // Extra check -- verbose
  G4NavigationHistory historyCopy2(history);
#endif

  // we need to do a couple of things
  // a) fetch the top-volume and translate to VecGeom
  const auto g4pvvol = history.GetTopVolume();
  assert(g4pvvol->IsReplicated() == false); // for the moment no replication

  // fetch the VecGeom version of this and dispatch to local VecGeom navigator
  const int replica  = 0; // fHistory.GetReplicaNo(l);
  const auto pv      = fG4VGLookup->G4ToVG(((MYG4VP *)g4pvvol)->getID(), replica);
  const auto exclvol = blockedVol ? fG4VGLookup->G4ToVG(((MYG4VP *)blockedVol)->getID(), replica) : nullptr;
  const auto lvol    = pv->GetLogicalVolume();

  // b) fetch the right acceleration structure for this volume
  const auto bestlevellocator = lvol->GetLevelLocator();

  using V3 = vecgeom::Vector3D<double>;
  const V3 vglocalpoint(localPoint[0], localPoint[1], localPoint[2]);

#if defined(G4LCOMPARE) && defined(VERBOSE_STEP)
  std::cout << " bestlevellocator " << bestlevellocator->GetName() << " navigating in " << g4pvvol->GetName() << " "
            << vglocalpoint << " blocked " << (exclvol ? blockedVol->GetName() : " NONE ") << "\n";
#endif
  // const V3 ld(localDirection[0], localDirection[1], localDirection[2]);

  V3 daughterlocalpoint;

  bool vgreturnvalue = false;

  vecgeom::VPlacedVolume const *newpvol = nullptr;
  V3 vglocaldirection(1., 0., 0.);

  if (globalDirection) {
    const auto ldir = history.GetTopTransform().TransformAxis(*globalDirection);
    vglocaldirection.Set(ldir[0], ldir[1], ldir[2]);
  }

  // d) select one or none from this list; adapt the history and localPoint appropriately
  bool newlevel =
      bestlevellocator->LevelLocateExclVol(lvol, exclvol, vglocalpoint, vglocaldirection, newpvol, daughterlocalpoint);
  assert((newpvol != exclvol) || exclvol == nullptr);

  // TODO: make a version that is also using the direction to take a final decision about containment

  // the new volume will be included in pvol
  if (newlevel) {
    // do back translation
    bool isreplica{false};
    int g4replicano{0};

    G4VPhysicalVolume *g4daughterpv = fG4VGLookup->VGToG4(newpvol->id(), g4replicano, isreplica);
    history.NewLevel(g4daughterpv, kNormal, g4daughterpv->GetCopyNo());
    // TODO: check if we need COPYNUMBER as in the original version ????  --> Answer Yes !! JA

    // CHECK that this corresponds well to local point
    localPoint[0] = daughterlocalpoint[0];
    localPoint[1] = daughterlocalpoint[1];
    localPoint[2] = daughterlocalpoint[2];
    vgreturnvalue = true;
  }
#ifdef G4LCOMPARE
  if (history.GetTopVolume() != historycopy.GetTopVolume() || (vgreturnvalue != returnvalue)) {
    std::cout << "------------------------------------------------------------------------------------------------\n";
    std::cout << " DIFFerence seen in results of G4-VecGeom-Voxel-Navigation::LevelLocate in mother vol "
              << g4pvvol
              /* << " name= " << g4pvvol->GetName() */
              << " localPointIn = " << localPointIn << " call# " << lcounter << "\n";
    printStateForLocate("G4 ", returnvalue, historycopy, localPointCopy);
    printStateForLocate("VG ", vgreturnvalue, history, localPoint);
    std::cout << " bestlevellocator " << bestlevellocator->GetName() << " navigating in " << g4pvvol->GetName()
              << "  Local-Pt " << vglocalpoint << " blocked " << (exclvol ? blockedVol->GetName() : " NONE ") << "\n";

    // EXTRA call for 'deep' debug prints  --- verbose output
    std::cout << "Candidates considered in G4VoxelNavigation: \n";
    G4ThreeVector localp2;
    int vl = GetVerboseLevel();
    SetVerboseLevel(1);
    bool returnvalue = this->G4VoxelNavigation::LevelLocate(historyCopy2, blockedVol, blockedNum, globalPoint,
                                                            globalDirection, pLocatedOnEdge, localp2);
    SetVerboseLevel(vl);
    std::cout << "------------------------------------------------------------------------------------------------\n";

    // Use G4 voxel navigator's values for return ...
    localPoint = localPointCopy;
    history    = historycopy;
  }

  return returnvalue;
#else

  return vgreturnvalue;
#endif
}
