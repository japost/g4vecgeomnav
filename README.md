# Introduction

This is a project demonstrating the use of a plugin allowing to run a Geant4 simulation 
with a VecGeom geometry and using the VecGeom
navigator modules. It is similar in spirit to the G4Root library maintained 
in the Virtual Monte Carlo framework (https://github.com/vmc-project/geant4_vmc)

## Main ideas

* A G4 user should be able to instantiate it's application as usual, then simply do something like
```c++
// make the G4VecGeomNavigator
auto nav = new G4VecGeomNavigator(g4geometry);                                                                                                                                                                           

// hooking the navigator into G4                                                                                                                                                                                                                                
auto trMgr = G4TransportationManager::GetTransportationManager();

trMgr->SetNavigatorForTracking(nav);
```
to benefit from this.

* The main classes are
  * `TG4VecGeomNavigator` : A class deriving from `G4Navigator`
  * `TG4VecGeomVoxelNavigation` : A class deriving from `G4VoxelNavigation`
  * `G4VecGeomConverter` : Convert the G4 geometry to VecGeom
  * `FastG4VecGeomLookup` : Lookup for fast syncing between G4 and VecGeom objects

## Additional notes

* For the purpose of easier benchmarking and comparison the original
  plugin for TGeo (G4Root) has been temporarily copied in source here
  (using tag 3-6 of geant4_vmc)

* The example "FullCMS" has been taken from the VMC repository and slightly
  modified
   * so that it runs with G4, TGeo or VecGeom
   * primary particles are identically generated in all cases to reduce statistical nois
   and make the comparison better
   
* The class layouts, APIs, etc are still under development and things may change or evolve
  heavily before coming to a first released version.

## Requirements

* VecGeom master branch
* Geant4 v10.5.1 (plus small additional patch)
* ROOT 6.18

## Installation and testing

* `mkdir build; cmake $PATH_TO_SOURCES; make`

* Run test application with VecGeom `full_cms -g geometry.gdml -m bench.g4 -v`
* Run test application with TGeo tracking `full_cms -g geometry.gdml -m bench.g4 -r`
* Run test application with native G4 `full_cms -g geometry.gdml -m bench.g4`

## References

* A talk at CHEP19 demonstration the benefit of the plugin
https://indico.cern.ch/event/773049/contributions/3474755/

