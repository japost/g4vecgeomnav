
#include "MyDetectorConstruction.hh"

#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4VisAttributes.hh"

#include "G4UniformMagField.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4VPhysicalVolume.hh"
#include "G4RunManager.hh"
#include "MyDetectorMessenger.hh"
#include "TG4RootDetectorConstruction.h"
#include "TGeoManager.h"
#include "TG4RootNavigator.h"
#include "G4RunManager.hh"
#include "G4TransportationManager.hh"
#include "G4PropagatorInField.hh"

#include "G4VecGeomConverter.h"
#include <VecGeom/management/GeoManager.h>
#include "TString.h"
#include "TError.h"

G4double MyDetectorConstruction::gFieldValue = 0.0;

MyDetectorConstruction::MyDetectorConstruction()
: fWorld(nullptr), fFieldMgr(nullptr), fUniformMagField(nullptr) , fDetectorMessenger(nullptr) {
  fGDMLFileName = "cms.gdml";
  fFieldValue   = 0.0;
  fDetectorMessenger = new MyDetectorMessenger(this);
}


MyDetectorConstruction::~MyDetectorConstruction() {
  delete fDetectorMessenger;
  if (fUniformMagField) {
    delete fUniformMagField;
  }
}


G4VPhysicalVolume* MyDetectorConstruction::Construct() {
  //  parser.SetOverlapCheck(true);
  G4TransportationManager *trMgr = G4TransportationManager::GetTransportationManager();
  assert(trMgr);

  if(fUseTGeo) {
    G4cout << "Using TGeo interface ..." << G4endl;
    auto geom = TGeoManager::Import(fGDMLFileName.c_str());
    auto constr = new TG4RootDetectorConstruction(geom);
    fWorld = constr->Construct();

    // connect the navigator
    // making the navigator
    auto* nav = new TG4RootNavigator(constr);

    // hooking the TGeo navigator
    trMgr->SetNavigatorForTracking(nav);
    G4FieldManager *fieldMgr =
        trMgr->GetPropagatorInField()->GetCurrentFieldManager();
    delete trMgr->GetPropagatorInField();
    trMgr->SetPropagatorInField(new G4PropagatorInField(nav, fieldMgr));
    trMgr->ActivateNavigator(nav);
    G4EventManager *evtMgr = G4EventManager::GetEventManager();
    if (evtMgr) {
      evtMgr->GetTrackingManager()->GetSteppingManager()->SetNavigator(nav);
    }
    Info("SetNavigator",
         "TG4RootGeomNavigator created and registered to G4TransportationManager");
  } else {
    fParser.Read(fGDMLFileName, false);
    fWorld = (G4VPhysicalVolume *)fParser.GetWorldVolume();

    // enabling 'check' mode in G4 Navigator
    G4Navigator *nav = trMgr->GetNavigatorForTracking();
    assert(nav);
    nav->CheckMode(true);
    std::cout << "Enabled Check mode in G4Navigator";

    // write back
    // fParser.Write("out.gdml", fWorld);
  }
  fFieldMgr = G4TransportationManager::GetTransportationManager()->GetFieldManager();
  fWorld->GetLogicalVolume()->SetVisAttributes(G4VisAttributes::GetInvisible());
  if (fWorld==0) {
    G4ExceptionDescription ed;
    ed << "World volume not set properly check your setup selection criteria or GDML input!"
       << G4endl;
    G4Exception( "MyDetectorConstruction::Construct()", "FULL_CMS_0000", FatalException, ed );
  }
  SetMagField();

  if (fUseVecGeom) {
    // This is converting the geometry to VecGeom and implicitely also setting
    // the navigator; We should pull this out really
    G4VecGeomConverter::Instance().SetVerbose(1);
    G4VecGeomConverter::Instance().ConvertG4Geometry(fWorld);
    G4cout << vecgeom::GeoManager::Instance().getMaxDepth() << "\n";
  }
  return fWorld;
}


void MyDetectorConstruction::SetMagField() {
  if (fUniformMagField ) {
    delete fUniformMagField;
  }
  if (std::abs(fFieldValue)>0.0) {
    // Apply a global uniform magnetic field along the Z axis.
    // Notice that only if the magnetic field is not zero, the Geant4
    // transportion in field gets activated.
    fUniformMagField = new G4UniformMagField(G4ThreeVector(0.0,0.0,fFieldValue));
    fFieldMgr->SetDetectorField(fUniformMagField);
    fFieldMgr->CreateChordFinder(fUniformMagField);
    G4cout << G4endl
           << " *** SETTING MAGNETIC FIELD : fieldValue = " << fFieldValue / tesla
           << " Tesla *** " << G4endl
	         << G4endl;

  } else {
    G4cout << G4endl
           << " *** NO MAGNETIC FIELD SET  *** " << G4endl
	         << G4endl;
  }
}
